import { Component, OnInit } from '@angular/core';
import {Hero} from '../hero';
import {HEROES} from '../mock-heroes';
import {HeroService} from '../hero.service';
import {MessageService} from '../message.service';
@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  heroes: Hero[];
  constructor( 
    private heroSerivce: HeroService
  ) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroSerivce.getHeroes().subscribe(h => this.heroes = h);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) {
      window.alert('Hero name must not empty');
      return;
    }
    this.heroSerivce.addHero({name} as Hero).subscribe(hero => {
      this.heroes.push(hero);
    });
  }

  delete(hero: Hero): void {
    if(window.confirm("Are you sure?"))
    {
      this.heroes = this.heroes.filter(x => x !== hero);
      this.heroSerivce.deleteHero(hero).subscribe();
    }    
  }

  
  
}
